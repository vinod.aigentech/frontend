import Vue from 'vue'
import App from './App.vue'
import {
  BootstrapVue,
  IconsPlugin
} from 'bootstrap-vue'
import VueRouter from 'vue-router';
import axios from 'axios';
import Vuex from 'vuex';
import home from './views/home';
import store from './views/store';
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue);
Vue.use(IconsPlugin)
Vue.use(VueRouter);
Vue.use(Vuex);
Vue.prototype.axios = axios;


const routes = [{
    path: '/',
    name: 'home',
    component: home
  },
  {
    path: '/store',
    name: 'store',
    component: store
  }
]

export const router = new VueRouter({
  mode: 'history',
  routes
});
Vue.prototype.routes = router;

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')